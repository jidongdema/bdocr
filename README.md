# ZYB-BDFaceRecon

## 简介
ZYB-BDFaceRecon插件是基于百度人脸活体认证SDK创建的uni插件。

### 准备
请前往[百度开发平台](http://ai.baidu.com/tech/ocr)申请账号，出于安全考虑，百度推荐使用授权文件方式进行开发者认证，请各位开发者申请应用之前务必认真阅读文档[http://ai.baidu.com/docs#/OCR-Android-SDK/7bb09719](http://ai.baidu.com/docs#/OCR-Android-SDK/7bb09719)。
然后进入 人脸识别 ---》 申请 活体认证sdk。


**下载License文件授权文件** <br>
应用创建成功之后，点击应用列表，选择已创建的应用对应的管理按钮，点击管理按钮 <br>

- “下载License文件-iOS（文字识别）”，将下载下来的License文件替换插件目录nativeplugins/ZYB-BDFaceRecon/ios/中的同名文件（ 目录没有自己创建）。放入百度后台取得的idl-license.face-ios，idl-key.face-ios 2个文件就可以了（百度后台下载的活体sdk中有idl-license.face-ios，idl-key.face-ios）。



### 须知

百度人脸识别对所有用户均提供每天有限次数的免费使用服务，如有更大需求，需开通付费。

当前插件基于网络使用，使用时请确保网络通畅。


##  此工程运行方法
- 工程已配置完成（注百度人脸库只支持 **真机运行** **真机运行** **真机运行**）


### Api 说明

**引用方式**

`const card = uni.requireNativePlugin('ZYB-BDFaceRecon');`

**方法说明**
1.初始化
`BrushFaceInit(options,callback)`
2.使用
`BrushFaceAsyncFunc(options,callback)`
> 点击会跳页扫描

参数说明
'maskType': 'Checkface',
'nameStr': '张xxx', //
'cardIdStr': '34xxxxx', //
'degree': '60',
'source':'1',//1.(公安系统 每天有一定免费次数) 2.(非公安系统 选择2就需要传imageUrl，也就是身份证正面照片url)
'imageUrl':''//身份证正面照片url ，source选择2才需要传此参数。

callback返回数据（数据为JSON格式，仅解释通用部分，其余部分参数不一一详释）

类型|	说明|
:--|:--|
result|	error（异常）、success（成功）、recognize（识别中）
识别成功后返回的图片base64格式

使用方法

```
<template>
    <view>
        <button type="primary" @click="checkFace()">点击人脸识别</button>
    </view>
</template>
引入
var faceSDK = uni.requireNativePlugin("ZYB-BDFaceRecon");

1.初始化
created() {
      //初始化
      // FACE_LICENSE_ID 在后台 -> 产品服务 -> 人脸识别 -> 客户端SDK管理查看，如果没有的话就新建一个
      // FACE_API_KEY ，FACE_SECRET_KEY 两个在后台 -> 产品服务 -> 人脸识别 -> 应用列表下面查看，如果没有的话就新建一个
      faceSDK.BrushFaceInit({
          'FACE_LICENSE_ID': 'xxxx-face-ios',
          'FACE_API_KEY': 'xxxxx', //
          'FACE_SECRET_KEY': 'xxxxxx'
        },
        (ret) => {
          console.log('ret :' + JSON.stringify(ret));
        })
    }

2.使用
  
    export default {
        data() {
            return {
            };
        },
        methods: {
            checkFace() {
               faceSDK.BrushFaceAsyncFunc({
                          'maskType': 'Checkface',
                          'nameStr': '张xx', //填写自己试试
                          'cardIdStr': '34xxx', //填写自己试试
                          'degree': '60',//及格线
                          'source':'1',//1.花钱(公安) 2.不花钱(非公安系统 选择2就需要传imageUrl，也就是身份证正面照片url)
                          'imageUrl':''//身份证正面照片url
                        },
                        (ret) => {
                          //ret :{"errorCode":"识别成功","state":"2","msg":"success","score":95.94256591796875}
                          console.log('ret :' + JSON.stringify(ret));
                          if (ret.state != "2") {
                            uni.showToast({
                              title: ret.errorCode,
                              icon: "none"
                            })
                          }
                        })
            }
        }
    }

<style>
</style>
```
